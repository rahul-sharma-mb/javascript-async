// const { reject } = require("lodash");

// function fetchRandomNumbers(callback){
//     console.log('Fetching number...');
//     setTimeout(() => {
//         let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
//         console.log('Received random number:', randomNum);
//         callback(randomNum);
//     }, (Math.floor(Math.random() * (5)) + 1) * 1000);
// }

// function fetchRandomString(callback){
//     console.log('Fetching string...');
//     setTimeout(() => {
//         let result           = '';
//         let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//         let charactersLength = characters.length;
//         for ( let i = 0; i < 5; i++ ) {
//            result += characters.charAt(Math.floor(Math.random() * charactersLength));
//         }
//         console.log('Received random string:', result);
//         callback(result);
//     }, (Math.floor(Math.random() * (5)) + 1) * 1000);
// }



// fetchRandomNumbers((randomNum) => console.log(randomNum))
// fetchRandomString((randomStr) => console.log(randomStr))



//-------------Promises----------------------//

const fetchNumbers= ()=>{
    return new Promise((resolve,reject)=>{
        console.log('Fetching number...');
        setTimeout(()=>{
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log("received random number",randomNum)
            resolve(randomNum)
        },(Math.floor(Math.random() * (5)) + 1) * 1000)
    })
    }

    const fetchString=()=>{
    return new Promise((resolve,reject)=>{
        console.log('Fetching string...');
        setTimeout(()=>{
            let result           = '';
            let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for ( let i = 0; i < 5; i++ ) {
               result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            console.log("received random string",result)
            resolve(result)

        },(Math.floor(Math.random() * (5)) + 1) * 1000)

    })

    }

    // //Task 1
    fetchNumbers().then(randomNum=>console.log('Received random number:', randomNum))
    fetchString().then(result=>console.log('Received random string:', result))


    // //Task 2
    fetchNumbers()
    .then(randomNum=>{
         let sum =0
         console.log('Received random number:', randomNum)
         sum+=randomNum
         console.log("resultant sum:",sum)
         return randomNum
    })
    .then((randomNum)=>{
            fetchNumbers().then((val)=>{
            console.log('Received random number:', val)
            val+=randomNum
            console.log("resultant sum:",val)
            })
        }).catch(error=>console.log(error))

    //task3
    let number= fetchNumbers()
    let string= fetchString()
    Promise.all([number,string]).then(valArr=> console.log(valArr[0]+valArr[1])).catch(error=>console.log(error))

    // //task4
    const task4= ()=>{
        let promises=[],sum=0
        for(let i=0;i<10;i++){
            promises.push(fetchNumbers())
        }
        Promise.all(promises).then(values=>{
           let a= values.reduce((acc,index)=>{
                return acc +index

            },0)
            console.log("Resultant sum",a)
        })
    }
    task4()


    // //____________________ASYNC-AWAIT_______________________//

    const fetchNumberAsync=  ()=>{
       return new Promise((resolve,reject)=>{
            console.log('Fetching number...');
            setTimeout(()=>{
                let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
                console.log("received random number:", randomNum)
                resolve(randomNum)
            },(Math.floor(Math.random() * (5)) + 1) * 1000)
        })
        }



        const fetchStringAsync=()=>{
            return  new Promise((resolve,reject)=>{
                console.log('Fetching string...');
                setTimeout(()=>{
                    let result           = '';
                    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                    let charactersLength = characters.length;
                    for ( let i = 0; i < 5; i++ ) {
                       result += characters.charAt(Math.floor(Math.random() * charactersLength));
                    }
                    console.log("received random String:", result)
                    resolve(result)

                },(Math.floor(Math.random() * (5)) + 1) * 1000)

            })

            }



    // //task 1
    const task1 = async ()=>{
         let randomNumber= await fetchNumberAsync()
         console.log("Received random number:",randomNumber)
         let randomString= await fetchStringAsync()
         console.log("Received random string:",randomString)
    }
    task1()


    //task 2
    const addTwice=async()=>{
        let firstNumber= await fetchNumberAsync()
        let secondNumber= await fetchNumberAsync()
        console.log("Resultant sum:", firstNumber+secondNumber)
    }
    //addTwice()


    // //task 3
    const numberAndString=async()=>{
        let number =  fetchNumberAsync()
        let string =  fetchStringAsync()
        let result= await Promise.all([number,string])
        console.log("concatenated string:",result[0]+result[1])
    }
    numberAndString()

    // //task 4


    const addTenTimesAsync= async()=>{
        let promises=[]
        for(let i=0;i<10;i++){
            promises.push(fetchNumbers())
        }
        let result = await Promise.all(promises)
        let sum=result.reduce((acc,current)=>{
            return acc+current
        },0)
        console.log("Resultant Sum",sum)
    }
    addTenTimesAsync()
